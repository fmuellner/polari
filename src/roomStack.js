import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';
import Tp from 'gi://TelepathyGLib';

import AccountsMonitor from './accountsMonitor.js';
import ChatView from './chatView.js';
import EntryArea from './entryArea.js';
import { MessageInfoBar } from './appNotifications.js';
import RoomManager from './roomManager.js';

export default GObject.registerClass({
    Properties: {
        'entry-area-height': GObject.ParamSpec.uint(
            'entry-area-height', 'entry-area-height', 'entry-area-height',
            GObject.ParamFlags.READABLE,
            0, GLib.MAXUINT32, 0),
        'view-height': GObject.ParamSpec.uint(
            'view-height', 'view-height', 'view-height',
            GObject.ParamFlags.READABLE,
            0, GLib.MAXUINT32, 0),
    },
}, class RoomStack extends Gtk.Stack {
    constructor(params) {
        super(params);

        this._sizeGroup = new Gtk.SizeGroup({ mode: Gtk.SizeGroupMode.VERTICAL });
        this._rooms = new Map();

        this._roomManager = RoomManager.getDefault();

        this._roomAddedId =
            this._roomManager.connect('room-added', this._roomAdded.bind(this));
        this._roomRemovedId =
            this._roomManager.connect('room-removed', this._roomRemoved.bind(this));
        this._roomManager.rooms.forEach(r => this._roomAdded(this._roomManager, r));

        this.add_named(new ChatPlaceholder(this._sizeGroup), 'placeholder');

        this._entryAreaHeight = 0;
        this._viewHeight = 0;

        this.connect('destroy', () => {
            this._roomManager.disconnect(this._roomAddedId);
            this._roomManager.disconnect(this._roomRemovedId);
        });
    }

    vfunc_realize() {
        super.vfunc_realize();

        const toplevel = this.get_root();

        this._toplevelSignals = [
            toplevel.connect('notify::active-room',
                this._activeRoomChanged.bind(this)),
            toplevel.connect('active-room-state-changed',
                this._updateSensitivity.bind(this)),
        ];
        this._activeRoomChanged();
        this._updateSensitivity();
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();

        const toplevel = this.get_root();
        this._toplevelSignals.forEach(id => toplevel.disconnect(id));
        this._toplevelSignals = [];
    }

    vfunc_size_allocate(width, height, baseline) {
        super.vfunc_size_allocate(width, height, baseline);

        const [firstEntry] =
            this._sizeGroup.get_widgets().filter(w => w.get_mapped());
        const entryHeight = firstEntry
            ? firstEntry.get_allocated_height() - 1 : 0;
        if (this._entryAreaHeight !== entryHeight) {
            this._entryAreaHeight = entryHeight;
            this.notify('entry-area-height');
        }

        const viewHeight = this.get_allocated_height() - entryHeight;
        if (this._viewHeight !== viewHeight) {
            this._viewHeight = viewHeight;
            this.notify('view-height');
        }
    }

    // eslint-disable-next-line camelcase
    get entry_area_height() {
        return this._entryAreaHeight;
    }

    // eslint-disable-next-line camelcase
    get view_height() {
        return this._viewHeight;
    }

    _addView(id, view) {
        this._rooms.set(id, view);
        this.add_named(view, id);
    }

    _roomAdded(roomManager, room) {
        this._addView(room.id, new RoomView(room, this._sizeGroup));
    }

    _roomRemoved(roomManager, room) {
        const view = this._rooms.get(room.id);
        this._rooms.delete(room.id);

        this.remove(view);
        view.run_dispose();
    }

    _activeRoomChanged() {
        const room = this.get_root().active_room;
        this.set_visible_child_name(room ? room.id : 'placeholder');
    }

    _updateSensitivity() {
        const room = this.get_root().active_room;
        if (!room)
            return;
        let sensitive = room && room.channel;
        this._rooms.get(room.id).inputSensitive = sensitive;
    }
});

const SavePasswordConfirmationBar = GObject.registerClass(
class SavePasswordConfirmationBar extends MessageInfoBar {
    constructor(room) {
        let title = _('Should the password be saved?');
        let subtitle = vprintf(
            _('Identification will happen automatically the next time you connect to %s'),
            room.account.display_name);
        super({ title, subtitle });

        this._room = room;

        this.connect('destroy', this._onDestroy.bind(this));

        this.add_button(_('_Save Password'), Gtk.ResponseType.ACCEPT).set({
            action_name: 'app.save-identify-password',
            action_target: new GLib.Variant('o', this._room.account.object_path),
        });

        this._identifySentId = this._room.connect('identify-sent', () => {
            this.revealed = true;
        });
    }

    on_response(response) {
        if (response === Gtk.ResponseType.ACCEPT)
            return;

        let app = Gio.Application.get_default();
        let target = new GLib.Variant('o', this._room.account.object_path);
        app.lookup_action('discard-identify-password').activate(target);
    }

    _onDestroy() {
        if (this._identifySentId)
            this._room.disconnect(this._identifySentId);
        this._identifySentId = 0;
    }
});

const ChannelErrorBar = GObject.registerClass(
class ChannelErrorBar extends MessageInfoBar {
    constructor(room) {
        super({ title: _('Failed to join the room') });

        this._room = room;

        this.add_button(_('_Retry'), Gtk.ResponseType.ACCEPT).set({
            action_name: 'app.reconnect-room',
            action_target: new GLib.Variant('s', this._room.id),
        });


        this.connect('destroy', this._onDestroy.bind(this));

        this._identifyError = this._room.connect('notify::channel-error', () => {
            if (this._room.channel_error === '') {
                this.revealed = false;
                return;
            }
            this._updateLabels();
            this.revealed = true;
        });
    }

    _updateLabels() {
        let text;

        switch (this._room.channel_error) {
        case Tp.error_get_dbus_name(Tp.Error.CHANNEL_FULL):
            text = _('The room is full.');
            break;
        case Tp.error_get_dbus_name(Tp.Error.CHANNEL_BANNED):
            text = _('You have been banned from the room.');
            break;
        case Tp.error_get_dbus_name(Tp.Error.CHANNEL_INVITE_ONLY):
            text = _('The room is invite-only.');
            break;
        case Tp.error_get_dbus_name(Tp.Error.CHANNEL_KICKED):
            text = _('You have been kicked from the room.');
            break;
        default:
            text = _('It is not possible to join the room now, but you can retry later.');
        }

        this.subtitle = text;
    }

    _onDestroy() {
        if (this._identifyError)
            this._room.disconnect(this._identifyError);
    }
});

const ChatPlaceholder = GObject.registerClass(
class ChatPlaceholder extends Gtk.Overlay {
    _accountsMonitor = AccountsMonitor.getDefault();

    constructor(sizeGroup) {
        super();

        let image = new Gtk.Image({
            icon_name: 'org.gnome.Polari-symbolic',
            pixel_size: 96, halign: Gtk.Align.END,
            margin_end: 14,
        });

        let title = new Gtk.Label({
            use_markup: true,
            halign: Gtk.Align.START,
            margin_start: 14,
        });
        title.label = `<span letter_spacing="4500">${_('Polari')}<${'/'}span>`;
        title.add_css_class('polari-background-title');

        let description = new Gtk.Label({
            label: _('Join a room using the + button.'),
            halign: Gtk.Align.CENTER, wrap: true,
            margin_top: 24, use_markup: true,
        });
        description.add_css_class('polari-background-description');

        let inputPlaceholder = new Gtk.Box({ valign: Gtk.Align.END });
        sizeGroup.add_widget(inputPlaceholder);

        let grid = new Gtk.Grid({
            column_homogeneous: true,
            can_focus: false,
            column_spacing: 18,
            hexpand: true,
            vexpand: true,
            valign: Gtk.Align.CENTER,
        });
        grid.add_css_class('polari-background');
        grid.attach(image, 0, 0, 1, 1);
        grid.attach(title, 1, 0, 1, 1);
        grid.attach(description, 0, 1, 2, 1);
        this.set_child(grid);
        this.add_overlay(inputPlaceholder);
    }
});

const RoomView = GObject.registerClass(
class RoomView extends Gtk.Overlay {
    constructor(room, sizeGroup) {
        super({ name: `RoomView ${room.display_name}` });

        let box = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        this.set_child(box);

        if (room.type === Tp.HandleType.CONTACT)
            this.add_overlay(new SavePasswordConfirmationBar(room));

        this.add_overlay(new ChannelErrorBar(room));

        this._view = new ChatView(room);
        box.append(this._view);

        this._entryArea = new EntryArea({
            room,
            sensitive: false,
        });
        box.append(this._entryArea);

        this._view.bind_property('max-nick-chars',
            this._entryArea, 'max-nick-chars',
            GObject.BindingFlags.SYNC_CREATE);
        sizeGroup.add_widget(this._entryArea);

        this._view.connect('text-dropped', (view, text) => {
            this._entryArea.pasteText(text, text.split('\n').length);
        });
        this._view.connect('image-dropped', (view, image) => {
            this._entryArea.pasteImage(image);
        });
        this._view.connect('file-dropped', (view, file) => {
            this._entryArea.pasteFile(file);
        });
    }

    set inputSensitive(sensitive) {
        this._entryArea.sensitive = sensitive;
    }
});
